<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ArmaAdmin extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('Arma_model', 'model', TRUE);
    }

    function index()
    {
        $this->load->helper('form');
        $data['titulo'] = "Lord of the Shirtz | Cadastro de Armas";
        $data['armas'] = $this->model->listar();
        $this->load->view('arma_view.php', $data);
    }

    function inserir() {

        $this->load->library('form_validation');

        $this->form_validation->set_error_delimiters('<span>', '</span>');

        $this->form_validation->set_rules('name', 'Nome', 'required|max_length[40]');
        $this->form_validation->set_rules('attack', 'Ataque', 'trim|required');
        $this->form_validation->set_rules('defense', 'Defesa', 'trim|required');
        $this->form_validation->set_rules('damage', 'dano', 'trim|required');

        if ($this->form_validation->run() === FALSE) {
            $this->index();
        } else {
            $data['name'] = $this->input->post('name');
            $data['attack'] = $this->input->post('attack');
            $data['defense'] = $this->input->post('defense');
            $data['damage'] = $this->input->post('damage');

            $this->load->model('Arma_model', 'model', TRUE);

            if ($this->model->inserir($data)) {
                redirect('admin/ArmaAdmin');
            } else {
                log_message('error', 'Erro ao inserir o personagem.');
            }
        }
    }

    function editar($id)  {

        $data['titulo'] = "Lord of the Shirtz | Edição de Armas";
        $data['arma'] = $this->model->editar($id);
        $this->load->view('arma_edit', $data);
    }

    function atualizar() {

        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<span>', '</span>');

        $this->form_validation->set_rules('name', 'Nome', 'required|max_length[40]');
        $this->form_validation->set_rules('attack', 'Ataque', 'trim|required');
        $this->form_validation->set_rules('defense', 'Defesa', 'trim|required');
        $this->form_validation->set_rules('damage', 'dano', 'trim|required');

        if ($this->form_validation->run() === FALSE) {
            $this->editar($this->input->post('id'));
        } else {
            $data['id'] = $this->input->post('id');
            $data['name'] = $this->input->post('name');
            $data['attack'] = $this->input->post('attack');
            $data['defense'] = $this->input->post('defense');
            $data['damage'] = $this->input->post('damage');

            if ($this->model->atualizar($data)) {
                redirect('admin/ArmaAdmin');
            } else {
                log_message('error', 'Erro ao atualizar o personagem.');
            }
        }
    }

    function deletar($id) {

        if ($this->model->deletar($id)) {
            redirect('admin/ArmaAdmin');
        } else {
            log_message('error', 'Erro ao deletar o personagem.');
        }
    }
}