<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'controllers/My_Controller.php';
require APPPATH . 'beans/Dado.php';

class Game extends MY_Controller {

    private $players;

    public function __construct(){
        parent::__construct();
        $this->load->helper('url');

        $this->players = $this->session->players;
    }

	public function index()
	{

        $players = [];

        $this->load->model('Player_model');
        $this->load->model('Arma_model');

        $playersResult = $this->Player_model->listar();

        for($i=0; $i < count($playersResult); $i++){

            $armaResult = $this->Arma_model->getArma($playersResult[$i]->weapon);
            $arma = new Arma(
                $armaResult[0]->id,
                $armaResult[0]->name,
                $armaResult[0]->attack,
                $armaResult[0]->defense,
                $armaResult[0]->damage

            );

            $players[] = new Player(
                $playersResult[$i]->id,
                $playersResult[$i]->name,
                $playersResult[$i]->lifePoints,
                $playersResult[$i]->strength,
                $playersResult[$i]->agility,
                $arma
            );
        }

        // Salva os players na sessao
        $this->session->set_userdata('players', $players);



        $css = Array(
            'classStart' => '',
            'classRestart' => 'hide',
            'classP1Attack' => 'hide',
            'classP2Attack' => 'hide'
        );

		$this->load->view('game_index',
            array('players' => $this->session->userdata('players'),
                 'mensagens' => '',
                'css' => $css)
        );
	}

    public function iniciativa(){

        $css = Array(
            'classStart' => 'hide',
            'classRestart' => 'hide',
            'classP1Attack' => 'hide',
            'classP2Attack' => 'hide'
        );

        // ninguem atacou ainda
        $this->players[0]->setPlayedThisTurn(false);
        $this->players[1]->setPlayedThisTurn(false);

        // calcula a inciativa total do jogador 1
        $valorDado[] = Dado::rolar(20);
        $iniciativaTotal[0] = $valorDado[0] + $this->players[0]->getAgilidade();
        $this->players[0]->setIniciativa($iniciativaTotal[0]);

        // calcula a iniciativa total do jogador 2
        $valorDado[] = Dado::rolar(20);
        $iniciativaTotal[1] = $valorDado[1] + $this->players[1]->getAgilidade();
        $this->players[1]->setIniciativa($iniciativaTotal[1]);

        $mensagens = $this->players[0]->getNome() . " rola 1d20 para iniciativa.\n";
        $mensagens .= "Valor obtido: " . $valorDado[0] ."\n";
        $mensagens .= "Soma-se o seu fator de agilidade: " . $this->players[0]->getAgilidade() . "\n";
        $mensagens .= "<b>INICIATIVA TOTAL ( ".$this->players[0]->getNome()." ): " . $this->players[0]->getIniciativa() . "</b>\n";
        $mensagens .= "\n";
        $mensagens .= $this->players[1]->getNome() . " rola 1d20 para iniciativa.\n";
        $mensagens .= "Valor obtido: " . $valorDado[1] ."\n";
        $mensagens .= "Soma-se o seu fator de agilidade: " . $this->players[1]->getAgilidade() . "\n";
        $mensagens .= "<b>INICIATIVA TOTAL ( ".$this->players[1]->getNome()." ): " . $this->players[1]->getIniciativa() . "</b>\n";
        $mensagens .= "\n\n";


        if($this->players[0]->getIniciativa() > $this->players[1]->getIniciativa())
        {
            $mensagens .= "<b>" . $this->players[0]->getNome() . " VENCEU E IRÁ ATACAR AGORA!! </b>\n";
            $css = Array(
                'classStart' => 'hide',
                'classRestart' => 'hide',
                'classP1Attack' => '',
                'classP2Attack' => 'hide'
            );
        }
        elseif($this->players[0]->getIniciativa() < $this->players[1]->getIniciativa())
        {
            // Player2 venceu
            $mensagens .= "<b>" . $this->players[1]->getNome() . " VENCEU E IRÁ ATACAR AGORA!! </b>\n";
            $css = Array(
                'classStart' => 'hide',
                'classRestart' => 'hide',
                'classP1Attack' => 'hide',
                'classP2Attack' => ''
            );

        }else
        {
            // empate
            $mensagens .= "<b>EMPATE!! RODAR INICIATIVA NOVAMENTE! </b>\n";
            $css = Array(
                'classStart' => '',
                'classRestart' => 'hide',
                'classP1Attack' => 'hide',
                'classP2Attack' => 'hide'
            );

        }


        $this->session->set_userdata('players', $this->players);
        $this->load->view('game_index',
            array('players' => $this->session->userdata('players'),
                'mensagens' => nl2br($mensagens),
                'css' => $css
            )
        );
    }

    public function attack($playerIndex){

        $attackingPlayer = $defendingPlayer = null;
        $gameOver = false;

        // Quem esta atacando e quem esta defendendo primeiro?
        if($playerIndex / 1 == 0){
            $attackingPlayer = $this->players[0];
            $defendingPlayer = $this->players[1];
        }else{
            $attackingPlayer = $this->players[1];
            $defendingPlayer = $this->players[0];
        }

        // salva que esse personagem ja atacou
        $attackingPlayer->setPlayedThisTurn(true);
        $response = $attackingPlayer->atacar($defendingPlayer);


        $mensagens = $attackingPlayer->getNome() . " irá atacar agora!\n";
        $mensagens .= $attackingPlayer->getNome() . " lança um dado para calcular o ataque!\n";
        $mensagens .= "Resultado obtido: " . $response['valorDadoAtaque'] ."\n";
        $mensagens .= "Soma-se ao valor do dado, o fator de agilidade (".$attackingPlayer->getAgilidade().") " .
            "+ fator de ataque da arma (".$attackingPlayer->getArma()->getAtaque().")\n";
        $mensagens .= "<b>VALOR TOTAL DO ATAQUE: " . $response['ataqueTotal'] ."</b>\n";

        $mensagens .= "\n";

        $mensagens .= $defendingPlayer->getNome() . " irá se defender agora!\n";
        $mensagens .= $defendingPlayer->getNome() . " lança um dado para calcular o ataque!\n";
        $mensagens .= "Resultado obtido: " . $response['valorDadoDefesa'] ."\n";
        $mensagens .= "Soma-se ao valor do dado, o fator de agilidade (".$defendingPlayer->getAgilidade().") " .
            "+ fator de ataque da arma (".$defendingPlayer->getArma()->getDefesa().")\n";
        $mensagens .= "<b>VALOR TOTAL DA DEFESA: " . $response['defesaTotal'] ."</b>\n";

        $mensagens .= "\n";

        if($response['danoTotal'] > 0){
            // defensor sofrerá dano
            $mensagens .= $defendingPlayer->getNome() . " não conseguiu se defender do ataque!\n";
            $mensagens .= "Calculando o dano sofrido...\n";

            $mensagens .= $attackingPlayer->getNome() . " lança um dado para calcular o dano!\n";
            $mensagens .= "Resultado obtido: " . $response['valorDadoDano'] ."\n";
            $mensagens .= "Soma-se ao valor do dado, o fator de força (".$attackingPlayer->getForca().") \n";

            $mensagens .= "<b>" . $defendingPlayer->getNome() . " sofreu " . $response['danoTotal'] ." pontos de dano!</b>\n";
            $mensagens .= "<b>" . $defendingPlayer->getNome() . " possuia " . $response['defensorPdvOld'] ." pontos de vida e agora possui " . $response['defensorPdvNew'] . "!</b>\n";


            // TODO: Ja acabou, Jessica???
            if($defendingPlayer->getVida() <= 0)
            {
                // ja acabou......
                $gameOver = true;
                $mensagens .= "<H1>" . $attackingPlayer->getNome() . " VENCEU A BATALHA!!!!! </H1>";

            }

        }else{
            $mensagens .= "<b>" . $defendingPlayer->getNome() . " conseguiu se defender e não sofrerá qualquer dano!!</b>\n";
        }

        if($defendingPlayer->isPlayedThisTurn()) {
            // Ambos já atacaram
            $css = Array(
                'classStart' => '',
                'classRestart' => 'hide',
                'classP1Attack' => 'hide',
                'classP2Attack' => 'hide'
            );
        }else{
            // falta alguem atacar
            if($playerIndex == 0){
                $css = Array(
                    'classStart' => 'hide',
                    'classRestart' => 'hide',
                    'classP1Attack' => 'hide',
                    'classP2Attack' => ''
                );
            }else{
                $css = Array(
                    'classStart' => 'hide',
                    'classRestart' => 'hide',
                    'classP1Attack' => '',
                    'classP2Attack' => 'hide'
                );
            }
        }

        if($gameOver){

            $css = Array(
                'classStart' => 'hide',
                'classRestart' => '',
                'classP1Attack' => 'hide',
                'classP2Attack' => 'hide'
            );

        }


        $this->session->set_userdata('players', $this->players);
        $this->load->view('game_index',
            array('players' => $this->session->userdata('players'),
                'mensagens' => nl2br($mensagens),
                'css' => $css
            )
        );
    }
}