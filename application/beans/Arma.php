<?php

class Arma
{

    private $id;
    private $nome;
    private $ataque;
    private $defesa;
    private $dano;

    public function Arma($id, $nome, $ataque, $defesa, $dano){
        $this->id = $id;
        $this->nome = $nome;
        $this->ataque = $ataque;
        $this->defesa = $defesa;
        $this->dano = $dano;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getAtaque()
    {
        return $this->ataque;
    }

    /**
     * @param mixed $ataque
     */
    public function setAtaque($ataque)
    {
        $this->ataque = $ataque;
    }

    /**
     * @return mixed
     */
    public function getDano()
    {
        return $this->dano;
    }

    /**
     * @param mixed $dano
     */
    public function setDano($dano)
    {
        $this->dano = $dano;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDefesa()
    {
        return $this->defesa;
    }

    /**
     * @param mixed $defesa
     */
    public function setDefesa($defesa)
    {
        $this->defesa = $defesa;
    }
}