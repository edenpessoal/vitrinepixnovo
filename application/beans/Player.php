<?php

require APPPATH . 'beans/Arma.php';

class Player
{

    private $id;
    private $nome;
    private $vida;
    private $forca;
    private $agilidade;
    private $arma;

    private $iniciativa = 0;
    private $playedThisTurn = true;

    public function Player($id, $nome, $vida, $forca, $agilidade, Arma $arma){
        $this->id = $id;
        $this->nome = $nome;
        $this->vida = $vida;
        $this->forca = $forca;
        $this->agilidade = $agilidade;
        $this->arma = $arma;
    }


    /**
     * Método que executa um ataque sob o $defendingPlayer
     *
     * @uses Arma
     * @uses Dice
     *
     * @param Player $defendingPlayer
     *
     * @return Array
     */
    public function atacar(Player $defendingPlayer){

        $valorDadoDano = 0;
        $danoTotal = 0;
        $defensorPdvOld = $defendingPlayer->getVida();

        // Rola-se um 1d20 + fator de agilidade + fator de ataque da arma do atacante
        $valorDadoAtaque = Dado::rolar(20);
        $ataqueTotal = $valorDadoAtaque + $this->getAgilidade() + $this->getArma()->getAtaque();

        // Rola-se 1d20 + fator de agilidade + fator de defesa da arma do defensor
        $valorDadoDefesa = Dado::rolar(20);
        $defesaTotal = $valorDadoDefesa + $defendingPlayer->getAgilidade() + $defendingPlayer->getArma()->getDefesa();

        if($valorDadoAtaque > $valorDadoDefesa){
            // Ataque nao foi defendido.
            // Calcula-se o dano.
            $valorDadoDano = Dado::rolar( $this->getArma()->getDano() );
            $danoTotal = $valorDadoDano[0] + $this->getForca();

            // Retira pontos de vida do defensor
            $defendingPlayer->sofrerDano($danoTotal);
        }

        // Sanity Check
        if($defendingPlayer->getVida() != ($defensorPdvOld - $danoTotal)){
            // Algo muito errado aconteceu.
            // Para a execucao do programa
            throw new RuntimeException;
        }

        return Array(
            'valorDadoAtaque' => $valorDadoAtaque,
            'valorDadoDefesa' => $valorDadoDefesa,
            'valorDadoDano' => $valorDadoDano,
            'ataqueTotal' => $ataqueTotal,
            'defesaTotal' => $defesaTotal,
            'danoTotal' => $danoTotal,
            'defensorPdvOld' => $defensorPdvOld,
            'defensorPdvNew' => $defendingPlayer->getVida()
        );
    }

    /**
     * @param mixed $vida
     */
    public function sofrerDano($dano)
    {
        $this->vida -= $dano;
    }


    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getVida()
    {
        return $this->vida;
    }

    /**
     * @param mixed $vida
     */
    public function setVida($vida)
    {
        $this->vida = $vida;
    }

    /**
     * @return mixed
     */
    public function getForca()
    {
        return $this->forca;
    }

    /**
     * @param mixed $forca
     */
    public function setForca($forca)
    {
        $this->forca = $forca;
    }

    /**
     * @return mixed
     */
    public function getAgilidade()
    {
        return $this->agilidade;
    }

    /**
     * @param mixed $agilidade
     */
    public function setAgilidade($agilidade)
    {
        $this->agilidade = $agilidade;
    }

    /**
     * @return mixed
     */
    public function getArma()
    {
        return $this->arma;
    }

    /**
     * @param mixed $arma
     */
    public function setArma(Arma $arma)
    {
        $this->arma = $arma;
    }

    /**
     * @return int
     */
    public function getIniciativa()
    {
        return $this->iniciativa;
    }

    /**
     * @param int $iniciativa
     */
    public function setIniciativa($iniciativa)
    {
        $this->iniciativa = $iniciativa;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return boolean
     */
    public function isPlayedThisTurn()
    {
        return $this->playedThisTurn;
    }

    /**
     * @param boolean $playedThisTurn
     */
    public function setPlayedThisTurn($playedThisTurn)
    {
        $this->playedThisTurn = $playedThisTurn;
    }


}