<?php

class Player_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function inserir($data) {
        return $this->db->insert('player', $data);
    }

    function listar() {
//        $query = $this->db->get('player');
        $query = $this->db->select('player.*, weapon.name as arma')
            ->from('player')
            ->join('weapon', 'player.weapon = weapon.id')
            ->get();

        return $query->result();
    }

    function editar($id) {
        $query = $this->db->select('player.*, weapon.name as arma')
            ->from('player')
            ->join('weapon', 'player.weapon = weapon.id')
            ->where('player.id', $id)
            ->get();

        return $query->result();
    }

    function atualizar($data) {
        $this->db->where('id', $data['id']);
        $this->db->set($data);
        return $this->db->update('player');
    }

    function deletar($id) {
        $this->db->where('id', $id);
        return $this->db->delete('player');
    }
}