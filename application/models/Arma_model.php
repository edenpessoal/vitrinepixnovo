<?php

class Arma_model extends CI_Model
{

    function __construct() {
        parent::__construct();
    }

    function getArma($id){
        $this->db->where('id', $id);
        $query = $this->db->get('weapon');
        return $query->result();
    }

    function inserir($data) {
        return $this->db->insert('weapon', $data);
    }

    function listar() {
        $query = $this->db->get('weapon');
        return $query->result();
    }

    function editar($id) {
        $this->db->where('id', $id);
        $query = $this->db->get('weapon');
        return $query->result();
    }

    function atualizar($data) {
        $this->db->where('id', $data['id']);
        $this->db->set($data);
        return $this->db->update('weapon');
    }

    function deletar($id) {
        $this->db->where('id', $id);
        return $this->db->delete('weapon');
    }
}