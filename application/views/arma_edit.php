<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title><?php echo $titulo; ?></title>
    <meta charset="UTF-8" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/estilo.css"/>
</head>
<body>
<?php echo form_open('/admin/ArmaAdmin/atualizar', 'id="form-arma"'); ?>

<input type="hidden" name="id" value="<?php echo $arma[0]->id; ?>"/>

<label for="name">Nome:</label><br/>
<input type="text" name="name" value="<?php echo $arma[0]->name; ?>"/>
<div class="error"><?php echo form_error('name'); ?></div>

<label for="attack">Ataque:</label><br/>
<input type="text" name="attack" value="<?php echo $arma[0]->attack; ?>"/>
<div class="error"><?php echo form_error('attack'); ?></div>

<label for="defense">Defesa:</label><br/>
<input type="text" name="defense" value="<?php echo $arma[0]->defense; ?>"/>
<div class="error"><?php echo form_error('defense'); ?></div>

<label for="damage">Dano:</label><br/>
<input type="text" name="damage" value="<?php echo $arma[0]->damage; ?>"/>
<div class="error"><?php echo form_error('damage'); ?></div>

<input type="submit" name="atualizar" value="Atualizar" />

<?php echo form_close(); ?>
</body>
</html>