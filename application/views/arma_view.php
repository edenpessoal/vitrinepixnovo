<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <title><?php echo $titulo; ?></title>
    <meta charset="UTF-8" />
</head>
<body>
<?php echo form_open('/admin/ArmaAdmin/inserir', 'id="form-arma"'); ?>

<label for="name">Nome:</label><br/>
<input type="text" name="name" value="<?php echo set_value('name'); ?>" required/>
<div class="error"><?php echo form_error('name'); ?></div>

<label for="attack">Ataque:</label><br/>
<input type="number" name="attack" value="<?php echo set_value('attack'); ?>" required />
<div class="error"><?php echo form_error('attack'); ?></div>

<label for="defense">Defesa:</label><br/>
<input type="number" name="defense" value="<?php echo set_value('defense'); ?>" required />
<div class="error"><?php echo form_error('defense'); ?></div>

<label for="damage">Dano:</label><br/>
<input type="number" name="damage" value="<?php echo set_value('damage'); ?>" required />
<div class="error"><?php echo form_error('damage'); ?></div>


<input type="submit" name="cadastrar" value="Cadastrar" />

<?php echo form_close(); ?>



<!-- Lista as Personaagens Cadastradas -->
<div id="grid-pessoas">
    <ul>
        <?php foreach($armas as $arma): ?>
            <li>
                <a title="Deletar" href="<?php echo base_url() . 'admin/ArmaAdmin/deletar/' . $arma->id; ?>" onclick="return confirm('Confirma a exclusão desta arma?')">X</a>
                <span> - </span>
                <a title="Editar" href="<?php echo base_url() . 'admin/ArmaAdmin/editar/' . $arma->id; ?>"><?php echo $arma->name; ?></a>
                <span> - </span>
                <span><?php echo $arma->attack; ?></span>
                <span> - </span>
                <span><?php echo $arma->defense; ?></span>
                <span> - </span>
                <span><?php echo $arma->damage; ?></span>
            </li>
        <?php endforeach ?>
    </ul>
</div>
<!-- Fim Lista -->

</body>
</html>