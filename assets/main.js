//$("#iniciativa-player1").click( function(e){
//
//    $(this).prop("disabled",true);
//
//    var callback = function(retorno, status){
//        $("#valor-iniciativa-player1").html( retorno[1] );
//        treatInitiative(0, retorno[1]);
//    };
//
//    rollDice(20, callback);
//});
//
//$("#iniciativa-player2").click( function(e){
//
//    $(this).prop("disabled",true);
//
//    var callback = function(retorno, status){
//        $("#valor-iniciativa-player2").html( retorno[1] );
//        treatInitiative(1, retorno[1]);
//    };
//
//    rollDice(20, callback);
//});

//var iniciativa = [];
//var playerTurno = null;
//
//function rollDice(faces, callback){
//    $.get("/dice/roll/faces/" +faces, callback);
//}
//
//function treatInitiative(player, valor){
//
//    iniciativa[player] = valor;
//
//    if(iniciativa.length >= 2){
//
//        var nomeJogador;
//
//        // ambos os jogadores ja rolaram os dados
//        // TODO: somar a agilidade do personagem
//        if(iniciativa[0] > iniciativa[1]){
//            playerTurno = 0;
//            // TODO: dados estarão no banco posteriormente
//            nomeJogador = "Humano";
//        }else{
//            // TODO: considerar o empate
//            playerTurno = 1;
//            nomeJogador = "Orc";
//        }
//
//        $("#title-iniciativa")
//            .html("O JOGADOR " + nomeJogador + " VENCEU A DISPUTA!!");
//
//        $("#iniciativa-prosseguir").prop("disabled",false);
//        $("#iniciar-iniciativa").hide();
//        $("#player1-atacar, #player2-atacar").removeClass("hide");
//
//    }
//}
//
//function attack(){
//
//}

$(function() {

    // busca as informacoes dos personagens
   $.get("/api/PlayerApi/players",
       function(result){

           // TODO: checar se há pelo menos dois personagens cadastrados

           // somente dois personagens sao permitidos
           for(var i=0; i<=1; i++){
               gameplay.players[i] = new player(
                   result[i].name,
                   result[i].lifePoints,
                   result[i].strength,
                   result[i].agility

                   // TODO: retornar o object weapon
               );
           }

           montaTela();

       }
   );


    $("#iniciativa-player1").click( function(e){
        $(this).prop("disabled",true);
        gameplay.initiative( gameplay.players[0] );
    });

    $("#iniciativa-player2").click( function(e){
        $(this).prop("disabled",true);
        gameplay.initiative( gameplay.players[1] );
    });

});

function montaTela(){

    $("#name1").html( gameplay.players[0].name );
    $("#name2").html( gameplay.players[1].name );
    $("#agility1").html( gameplay.players[0].agility );
    $("#agility2").html( gameplay.players[1].agility );
    $("#strength1").html( gameplay.players[0].strength );
    $("#strength2").html( gameplay.players[1].strength );
    $("#player1_pdv").html( gameplay.players[0].lifePoints );
    $("#player2_pdv").html( gameplay.players[1].lifePoints );

}